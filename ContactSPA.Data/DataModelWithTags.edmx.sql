
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/12/2017 13:11:26
-- Generated from EDMX file: C:\Users\Szzark\Desktop\Contact App\ContactSPA\ContactSPA.Data\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ContactApp];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ContactTelephone]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Telephones] DROP CONSTRAINT [FK_ContactTelephone];
GO
IF OBJECT_ID(N'[dbo].[FK_ContactEmail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Emails] DROP CONSTRAINT [FK_ContactEmail];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Contacts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contacts];
GO
IF OBJECT_ID(N'[dbo].[Telephones]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Telephones];
GO
IF OBJECT_ID(N'[dbo].[Emails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Emails];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Contacts'
CREATE TABLE [dbo].[Contacts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Organization] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Telephones'
CREATE TABLE [dbo].[Telephones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [Contact_Id] int  NOT NULL
);
GO

-- Creating table 'Emails'
CREATE TABLE [dbo].[Emails] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailAddress] nvarchar(max)  NOT NULL,
    [Contact_Id] int  NOT NULL
);
GO

-- Creating table 'Tags'
CREATE TABLE [dbo].[Tags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TagName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ContactTags'
CREATE TABLE [dbo].[ContactTags] (
    [Contact_Id] int  NOT NULL,
    [Tags_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Contacts'
ALTER TABLE [dbo].[Contacts]
ADD CONSTRAINT [PK_Contacts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Telephones'
ALTER TABLE [dbo].[Telephones]
ADD CONSTRAINT [PK_Telephones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Emails'
ALTER TABLE [dbo].[Emails]
ADD CONSTRAINT [PK_Emails]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Tags'
ALTER TABLE [dbo].[Tags]
ADD CONSTRAINT [PK_Tags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Contact_Id], [Tags_Id] in table 'ContactTags'
ALTER TABLE [dbo].[ContactTags]
ADD CONSTRAINT [PK_ContactTags]
    PRIMARY KEY CLUSTERED ([Contact_Id], [Tags_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Contact_Id] in table 'Telephones'
ALTER TABLE [dbo].[Telephones]
ADD CONSTRAINT [FK_ContactTelephone]
    FOREIGN KEY ([Contact_Id])
    REFERENCES [dbo].[Contacts]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContactTelephone'
CREATE INDEX [IX_FK_ContactTelephone]
ON [dbo].[Telephones]
    ([Contact_Id]);
GO

-- Creating foreign key on [Contact_Id] in table 'Emails'
ALTER TABLE [dbo].[Emails]
ADD CONSTRAINT [FK_ContactEmail]
    FOREIGN KEY ([Contact_Id])
    REFERENCES [dbo].[Contacts]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContactEmail'
CREATE INDEX [IX_FK_ContactEmail]
ON [dbo].[Emails]
    ([Contact_Id]);
GO

-- Creating foreign key on [Contact_Id] in table 'ContactTags'
ALTER TABLE [dbo].[ContactTags]
ADD CONSTRAINT [FK_ContactTags_Contact]
    FOREIGN KEY ([Contact_Id])
    REFERENCES [dbo].[Contacts]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Tags_Id] in table 'ContactTags'
ALTER TABLE [dbo].[ContactTags]
ADD CONSTRAINT [FK_ContactTags_Tags]
    FOREIGN KEY ([Tags_Id])
    REFERENCES [dbo].[Tags]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContactTags_Tags'
CREATE INDEX [IX_FK_ContactTags_Tags]
ON [dbo].[ContactTags]
    ([Tags_Id]);
GO

-- Populating initial data --

INSERT INTO [dbo].[Contacts] ([FirstName],[LastName],[Organization],[Address])
VALUES ('Igor', 'Ljustina', 'Home', 'Secret'),
	   ('Marko', 'Markovic', 'Unknown', 'Unknown')
	   
INSERT INTO [dbo].[Telephones] ([PhoneNumber],[Contact_Id])
VALUES ('0998532696', 1),
	   ('1231542355', 2)

INSERT INTO [dbo].[Emails] ([EmailAddress],[Contact_Id])
VALUES ('ljustina.igor@gmail.com', 1),
	   ('unknown@unknown.com', 2)


INSERT INTO [dbo].[Tags] ([TagName])
VALUES ('Home'),
	   ('office'),
	   ('Work'),
	   ('party')

	   INSERT INTO [dbo].[ContactTags] ([Tags_Id], [Contact_Id])
VALUES (1,1),
	   (1,2),
	   (2,2)
-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------