﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactSPA.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Ninject;
using System.Data.Entity.Infrastructure;

namespace ContactSPA.Services.ContactContentService
{
    public class ContactService : IContactService
    {
        private DataModelContainer _context;

        public ContactService(DataModelContainer context)
        {
            _context = context;
        }
        
        public List<Contact> GetContacts(string nameSearch, string tagSearch)
        {
            if (!string.IsNullOrWhiteSpace(tagSearch))
            {
                return GetContactListByTagAndName(nameSearch, tagSearch);
            }

            if (!string.IsNullOrWhiteSpace(nameSearch))
            {
                return GetContactListByName(nameSearch, _context.Contacts.Include("Tags"));
            }

            return _context.Contacts.OrderBy(a => a.FirstName).ToList();
        }
        
        public Contact GetContactDetails(int contactId)
        {
            return _context.Contacts
                 .Include("Telephones")
                 .Include("Emails")
                 .Include("Tags")
                 .FirstOrDefault(c => c.Id == contactId);
        }
        
        public Contact CreateContact(Contact contact)
        {
            ICollection<Tag> tags = GetNewContactTags(contact);
            contact.Tags = new List<Tag>();
            _context.Contacts.Add(contact);
            _context.SaveChanges();

            contact.Tags = tags;
            _context.SaveChanges();

            return contact;
        }

        public Contact UpdateContact(Contact contact)
        {
            var dbContact = _context.Contacts
                .Include("Telephones")
                .Include("Emails")
                .Include("Tags")
                .FirstOrDefault(c => c.Id == contact.Id);

            if (dbContact != null)
            {
                _context.Entry(dbContact).CurrentValues.SetValues(contact);

                ManageTelephones(contact, dbContact);

                ManageEmails(contact, dbContact);

                ManageTags(contact, dbContact);

                _context.SaveChanges();

                return dbContact;
            }
            return contact;
        }

        public string DeleteContact(int contactId)
        {
            var contact = _context.Contacts
               .Include("Telephones")
               .Include("Emails")
               .Include("Tags")
               .FirstOrDefault(c => c.Id == contactId);
            try
            {
                _context.Contacts.Remove(contact);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return "There was a problem deleting contact. Error : " + ex.InnerException;
            }

            return "Contact Deleted!";
        }
        
        #region Private Methods
        private ICollection<Tag> GetNewContactTags(Contact contact)
        {
            ICollection<Tag> tags = new List<Tag>();
            foreach (var tag in contact.Tags.ToList())
            {
                var existingTag = _context.Tags
                    .Include("Contacts")
                    .FirstOrDefault(e => e.TagName == tag.TagName);
                if (existingTag == null)
                {
                    tags.Add(tag);
                }
                else
                {
                    var tagContact = existingTag.Contacts.FirstOrDefault(c => c.Id == contact.Id);
                    if (tagContact == null)
                    {
                        tags.Add(existingTag);
                    }
                }
            }

            return tags;
        }

        private void ManageTags(Contact contact, Contact dbContact)
        {
            //if tag wasn't returned with the object, remove it from contact
            foreach (var tag in dbContact.Tags.ToList())
            {
                if (!contact.Tags.Any(e => e.TagName == tag.TagName))
                    dbContact.Tags.Remove(tag);
            }

            foreach (var tag in contact.Tags)
            {
                var existingTag = _context.Tags
                    .Include("Contacts")
                    .FirstOrDefault(e => e.TagName == tag.TagName);
                if (existingTag == null)
                {
                    dbContact.Tags.Add(tag);
                }
                else
                {
                    var tagContact = existingTag.Contacts.FirstOrDefault(c => c.Id == contact.Id);
                    if (tagContact == null)
                    {
                        dbContact.Tags.Add(existingTag);
                    }
                }
            }
        }

        private void ManageEmails(Contact contact, Contact dbContact)
        {
            //if email wasn't returned with the object, remove it from DB
            foreach (var email in dbContact.Emails.ToList())
            {
                if (!contact.Emails.Any(e => e.Id == email.Id))
                    _context.Emails.Remove(email);
            }

            foreach (var email in contact.Emails)
            {
                var existingEmail = _context.Emails
                    .FirstOrDefault(e => e.Id == email.Id && e.Id != default(int));
                if (existingEmail == null)
                {
                    dbContact.Emails.Add(email);
                }
                else
                {
                    _context.Entry(existingEmail).CurrentValues.SetValues(email);
                }
            }
        }

        private void ManageTelephones(Contact contact, Contact dbContact)
        {
            //if telephone wasn't returned with the object, remove it from DB
            foreach (var telephone in dbContact.Telephones.ToList())
            {
                if (!contact.Telephones.Any(t => t.Id == telephone.Id))
                    _context.Telephones.Remove(telephone);
            }

            foreach (var telephone in contact.Telephones)
            {
                var existingTelephone = _context.Telephones
                    .FirstOrDefault(t => t.Id == telephone.Id && t.Id != default(int));
                if (existingTelephone == null)
                {
                    dbContact.Telephones.Add(telephone);
                }
                else
                {
                    _context.Entry(existingTelephone).CurrentValues.SetValues(telephone);
                }
            }
        }

        private List<Contact> GetContactListByName(string nameSearch, DbQuery<Contact> contacts = null)
        {
            if (contacts == null)
            {
                return new List<Contact>();
            }

            if (!string.IsNullOrWhiteSpace(nameSearch))
            {
                return contacts
                    .Where(a => a.FirstName.ToLower().Contains(nameSearch.ToLower()) || a.LastName.ToLower().Contains(nameSearch.ToLower()))
                    .OrderBy(a => a.FirstName)
                    .ToList();
            }
            return contacts.ToList();
        }

        private List<Contact> GetContactListByTagAndName(string nameSearch, string tagSearch)
        {
            var filteredContacts = _context.Tags
                   .Include("Contacts")
                   .Where(a => tagSearch.ToLower().Contains(a.TagName.ToLower()))
                   .AsEnumerable()
                   .Select(c => c.Contacts
                   .Where(a => a.FirstName.ToLower().Contains(nameSearch.ToLower()) || a.LastName.ToLower().Contains(nameSearch.ToLower()))
                   .OrderBy(a => a.FirstName));

            List<Contact> contactList = new List<Contact>();
            foreach (var enumContactList in filteredContacts)
            {
                foreach (var contact in enumContactList)
                {
                    if (!contactList.Contains(contact))
                        contactList.Add(contact);
                }
            }

            return contactList;
        }
        #endregion
    }
}
