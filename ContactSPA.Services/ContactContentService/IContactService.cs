﻿using ContactSPA.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactSPA.Services.ContactContentService
{
    public interface IContactService
    {
        /// <summary>
        /// Filters database for contacts based on search criteria
        /// </summary>
        /// <param name="nameSearch">Search against first and last name of the contact.</param>
        /// <param name="tagSearch">Search agains contact tags.</param>
        /// <returns>List of filtered contacts</returns>
        List<Contact> GetContacts(string nameSearch, string tagSearch);

        /// <summary>
        /// Gets all contact details
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns>Full contact object</returns>
        Contact GetContactDetails(int contactId);

        /// <summary>
        /// Creates Contact from model
        /// </summary>
        /// <param name="contact"></param>
        /// <returns>Created Contact</returns>
        Contact CreateContact(Contact contact);

        /// <summary>
        /// Updates contacts based on given model
        /// </summary>
        /// <param name="contact">contact model</param>
        /// <returns>updated contact model</returns>
        Contact UpdateContact(Contact contact);

        /// <summary>
        /// Deletes contact with specific ID
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns>Message or error text</returns>
        string DeleteContact(int contactId);
    }
}
