﻿using ContactSPA.Data;
using ContactSPA.Services.ContactContentService;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContactSPA.Controllers
{
    public class ContactController : Controller
    {
        private IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            this._contactService = contactService;
        }

        public ActionResult GetContacts(string search = "", string tags = "")
        {
            var contacts = _contactService.GetContacts(search, tags);
            return Content(JsonConvert.SerializeObject(contacts, MvcApplication.JsonSerializerSettings), "application/json");
        }

        public ActionResult GetContactDetails(int id)
        {
            var contact = _contactService.GetContactDetails(id);

            return Content(JsonConvert.SerializeObject(contact, MvcApplication.JsonSerializerSettings), "application/json");
        }

        public ActionResult CreateContact(Contact contact)
        {
            if (ModelState.IsValid)
            {
                _contactService.CreateContact(contact);
            }
            return Content(JsonConvert.SerializeObject(contact, MvcApplication.JsonSerializerSettings), "application/json");

        }

        public ActionResult UpdateContact(Contact contact)
        {
            if (ModelState.IsValid)
            {
                Contact updatedContact = _contactService.UpdateContact(contact);
                return Content(JsonConvert.SerializeObject(updatedContact, MvcApplication.JsonSerializerSettings), "application/json");
            }
            return Content(JsonConvert.SerializeObject("Update Failed, model not valid", MvcApplication.JsonSerializerSettings), "application/json");
        }

        [HttpDelete]
        public ActionResult DeleteContact(int id)
        {
            string deletionMessage = _contactService.DeleteContact(id);
            return Content(JsonConvert.SerializeObject(deletionMessage, MvcApplication.JsonSerializerSettings), "application/json");
        }
    }
}
