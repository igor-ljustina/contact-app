webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  CONTACT APPLICATION\n  <div>\n    <router-outlet></router-outlet>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__contact_app_contact_list_contact_list_component__ = __webpack_require__("../../../../../src/app/contact-app/contact-list/contact-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contact_app_contact_details_contact_details_component__ = __webpack_require__("../../../../../src/app/contact-app/contact-details/contact-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__contact_app_contact_manage_contact_manage_component__ = __webpack_require__("../../../../../src/app/contact-app/contact-manage/contact-manage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__form_components_form_telephone_component__ = __webpack_require__("../../../../../src/app/form-components/form.telephone.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_6__contact_app_contact_list_contact_list_component__["a" /* ContactListComponent */] },
    { path: 'detail', component: __WEBPACK_IMPORTED_MODULE_7__contact_app_contact_details_contact_details_component__["a" /* ContactDetailComponent */] },
    { path: 'manage', component: __WEBPACK_IMPORTED_MODULE_8__contact_app_contact_manage_contact_manage_component__["a" /* ContactManageComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__contact_app_contact_list_contact_list_component__["a" /* ContactListComponent */],
                __WEBPACK_IMPORTED_MODULE_7__contact_app_contact_details_contact_details_component__["a" /* ContactDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_8__contact_app_contact_manage_contact_manage_component__["a" /* ContactManageComponent */],
                __WEBPACK_IMPORTED_MODULE_9__form_components_form_telephone_component__["a" /* TelephoneComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(routes, { useHash: true }),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/contact-app/contact-details/contact-details.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-details/contact-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"contact\">\r\n    <button (click)=\"backClicked()\">Back</button>\r\n    <a href=\"/#/manage?id={{contact.id}}\">Edit</a>\r\n    <button (click)=\"deleteContact(contact.id)\">Delete</button>\r\n\r\n    <div>\r\n        First Name: {{contact.firstName}}\r\n    </div>\r\n    <div>\r\n        Last Name: {{contact.lastName}}\r\n    </div>\r\n    <div>\r\n        Organization: {{contact.organization}}\r\n    </div>\r\n    <div>\r\n        Address: {{contact.address}}\r\n    </div>\r\n    <div>\r\n        Organization: {{contact.organization}}\r\n    </div>\r\n    <ul>\r\n        <li>\r\n            Emails\r\n        </li>\r\n        <li *ngFor=\"let email of contact.emails\">\r\n            {{email.emailAddress}}\r\n        </li>\r\n    </ul>\r\n    <ul>\r\n        <li>\r\n            Telephones\r\n        </li>\r\n        <li *ngFor=\"let telephone of contact.telephones\">\r\n            {{telephone.phoneNumber}}\r\n        </li>\r\n    </ul>\r\n    <ul>\r\n        <li>\r\n            Tags\r\n        </li>\r\n        <li *ngFor=\"let tag of contact.tags\">\r\n            {{tag.tagName}}\r\n        </li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-details/contact-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactDetailComponent = (function () {
    function ContactDetailComponent(route, http, _location) {
        this.route = route;
        this.http = http;
        this._location = _location;
    }
    ContactDetailComponent.prototype.backClicked = function () {
        this._location.back();
    };
    ContactDetailComponent.prototype.deleteContact = function (id) {
        if (id) {
            if (confirm('Are You sure?')) {
                this.http.delete("/contact/DeleteContact/" + id).subscribe(function (res) {
                    console.log(res);
                }, function (err) {
                    console.log("Error occured");
                });
            }
        }
    };
    ContactDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route
            .queryParams
            .subscribe(function (params) {
            _this.contactId = params['id'];
        });
        if (this.contactId) {
            this.http.get('/contact/GetContactDetails/' + this.contactId).subscribe(function (data) {
                _this.contact = data;
                console.log('details', data);
            });
        }
    };
    ContactDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/contact-app/contact-details/contact-details.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contact-app/contact-details/contact-details.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */]])
    ], ContactDetailComponent);
    return ContactDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/contact-app/contact-list/contact-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-list/contact-list.component.html":
/***/ (function(module, exports) {

module.exports = "<a href=\"/#/manage\">create</a>\r\n\r\n<div>\r\n    <label>Filter By Tags</label>\r\n    <input type=\"text\" (keyup)=\"onKey($event)\" />\r\n</div>\r\n\r\n<div>\r\n    <label>Filter By First name or last name</label>\r\n    <input type=\"text\" (keyup)=\"onKey($event)\"/>\r\n</div>\r\n\r\n\r\n<ul>\r\n    <li *ngFor=\"let contact of contactsList\">\r\n        <a href=\"/#/detail?id={{contact.id}}\">\r\n            {{contact.firstName}}\r\n        </a>\r\n        <div>\r\n            Tags :\r\n            <span *ngFor=\"let tag of contact.tags\">{{tag.tagName}}</span>\r\n        </div>\r\n    </li>\r\n</ul>"

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-list/contact-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactListComponent = (function () {
    function ContactListComponent(http) {
        this.http = http;
        this.tagSearch = '';
        this.nameSearch = '';
    }
    ContactListComponent.prototype.onKey = function (event) {
        var _this = this;
        this.nameSearch = event.target.value;
        this.http.get('/contact/GetContacts?search=' + this.nameSearch + '&tags=' + this.tagSearch).subscribe(function (data) {
            _this.contactsList = data;
        });
    };
    ContactListComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Make the HTTP request:
        this.http.get('/contact/GetContacts?search=' + this.nameSearch + '&tags=' + this.tagSearch).subscribe(function (data) {
            _this.contactsList = data;
        });
    };
    ContactListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/contact-app/contact-list/contact-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contact-app/contact-list/contact-list.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], ContactListComponent);
    return ContactListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/contact-app/contact-manage/contact-manage.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-manage/contact-manage.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"form\">\r\n    <form (ngSubmit)=\"onSubmit(form.value)\" [formGroup]=\"form\">\r\n        <label>First Name\r\n            <input type=\"text\" formControlName=\"firstName\">\r\n        </label>\r\n\r\n        <label>Last Name\r\n            <input type=\"text\" formControlName=\"lastName\">\r\n        </label>\r\n\r\n        <label>Organization\r\n            <input type=\"text\" formControlName=\"organization\">\r\n        </label>\r\n\r\n        <label>Address\r\n            <input type=\"text\" formControlName=\"address\">\r\n        </label>\r\n\r\n        <div formArrayName=\"telephones\">\r\n            <div *ngFor=\"let telephone of form.controls.telephones.controls; let i=index\">\r\n                <div>\r\n                    <span>Telephone {{i + 1}}</span>\r\n                    <span *ngIf=\"form.controls.telephones.controls.length > 1\" (click)=\"removeFromArray(i,'telephones')\">\r\n                        Delete Number\r\n                    </span>\r\n                </div>\r\n                <div [formGroupName]=\"i\">\r\n                        <phone [phoneGroup]=\"form.controls.telephones.controls[i]\"></phone>                    \r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div>\r\n            <span (click)=\"addPhone()\">\r\n                Add Another Telephone\r\n            </span>\r\n        </div>\r\n        <div formArrayName=\"emails\">\r\n            <div *ngFor=\"let email of form.controls.emails.controls; let i=index\">\r\n                <div>\r\n                    <span>Email {{i + 1}}</span>\r\n                    <span *ngIf=\"form.controls.emails.controls.length > 0\" (click)=\"removeFromArray(i,'emails')\">\r\n                        Delete Address\r\n                    </span>\r\n                </div>\r\n                <div [formGroupName]=\"i\">\r\n                    <div>\r\n                        <label>Email Address</label>\r\n                        <input type=\"text\" formControlName=\"emailAddress\">\r\n                        <input type=\"hidden\" formControlName=\"id\">\r\n                    </div>\r\n                    <div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div>\r\n            <span (click)=\"addEmail()\">\r\n                Add Another Email\r\n            </span>\r\n        </div>\r\n        <div formArrayName=\"tags\">\r\n                <div *ngFor=\"let tag of form.controls.tags.controls; let i=index\">\r\n                    <div>\r\n                        <span>Tag {{i + 1}}</span>\r\n                        <span *ngIf=\"form.controls.tags.controls.length > 0\" (click)=\"removeFromArray(i,'tags')\">\r\n                            Delete Tag\r\n                        </span>\r\n                    </div>\r\n                    <div [formGroupName]=\"i\">\r\n                        <div>\r\n                            <label>Tag Name</label>                            \r\n                            <input type=\"text\" formControlName=\"tagName\">\r\n                            <input type=\"hidden\" formControlName=\"id\">\r\n                        </div>\r\n                        <div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div>\r\n                <span (click)=\"addTag()\">\r\n                    Add Another Tag\r\n                </span>\r\n            </div>\r\n        <input type=\"submit\" value=\"Save Data\">\r\n    </form>\r\n\r\n    <div *ngIf=\"payLoad\" class=\"form-row\">\r\n        <strong>Saved the following values</strong>\r\n        <br>{{payLoad}}\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/contact-app/contact-manage/contact-manage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactManageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_contact_model__ = __webpack_require__("../../../../../src/app/data/contact.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactManageComponent = (function () {
    function ContactManageComponent(_fb, route, http, _location) {
        this._fb = _fb;
        this.route = route;
        this.http = http;
        this._location = _location;
        this.payLoad = '';
    }
    ContactManageComponent.prototype.backClicked = function () {
        this._location.back();
    };
    ContactManageComponent.prototype.onSubmit = function (post) {
        if (this.contactId) {
            this.loadedContact.emails = post.emails;
            this.loadedContact.telephones = post.telephones;
            this.loadedContact.firstName = post.firstName;
            this.loadedContact.lastName = post.lastName;
            this.loadedContact.organization = post.organization;
            this.loadedContact.address = post.address;
            this.loadedContact.tags = post.tags;
            this.http.post('/contact/UpdateContact', this.loadedContact).subscribe(function (res) {
                console.log('updated', res);
            }, function (err) {
                console.log("Error occured");
            });
        }
        else {
            this.loadedContact = new __WEBPACK_IMPORTED_MODULE_3__data_contact_model__["a" /* Contact */](post.firstName, post.lastName, post.address, post.organization, post.telephones, post.emails, post.tags);
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* RequestOptions */]({ headers: headers });
            this.http.post('/contact/CreateContact', this.loadedContact).subscribe(function (res) {
                console.log(res);
            }, function (err) {
                console.log("Error occured");
            });
        }
        console.log(this.loadedContact);
    };
    ContactManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route
            .queryParams
            .subscribe(function (params) {
            _this.contactId = params['id'];
        });
        if (this.contactId) {
            this.http.get('/contact/GetContactDetails/' + this.contactId).subscribe(function (data) {
                _this.loadedContact = data;
                _this.form = _this._fb.group({
                    firstName: [_this.loadedContact.firstName, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    lastName: [_this.loadedContact.lastName, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    organization: [_this.loadedContact.organization, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    address: [_this.loadedContact.address, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    telephones: _this._fb.array([]),
                    emails: _this._fb.array([]),
                    tags: _this._fb.array([])
                });
                _this.initEmails(_this.loadedContact);
                _this.initTelephones(_this.loadedContact);
                _this.initTags(_this.loadedContact);
            });
        }
        else {
            this.form = this._fb.group({
                firstName: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                lastName: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                organization: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                address: ['', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                telephones: this._fb.array([]),
                emails: this._fb.array([]),
                tags: this._fb.array([])
            });
            this.initEmails(null);
            this.initTelephones(null);
            this.initTags(null);
        }
    };
    ContactManageComponent.prototype.addPhone = function () {
        var control = this.form.controls['telephones'];
        control.push(this._fb.group({
            phoneNumber: [''],
            id: [0]
        }));
    };
    ContactManageComponent.prototype.addEmail = function () {
        var control = this.form.controls['emails'];
        control.push(this._fb.group({
            emailAddress: [''],
            id: [0]
        }));
    };
    ContactManageComponent.prototype.addTag = function () {
        var control = this.form.controls['tags'];
        control.push(this._fb.group({
            tagName: [''],
            id: [0]
        }));
    };
    ContactManageComponent.prototype.removeFromArray = function (i, array) {
        var control = this.form.controls[array];
        control.removeAt(i);
    };
    ContactManageComponent.prototype.initTelephones = function (contact) {
        var _this = this;
        if (contact && contact.telephones.length > 0) {
            var control_1 = this.form.controls['telephones'];
            contact.telephones.forEach(function (telephone) {
                control_1.push(_this._fb.group({
                    phoneNumber: [telephone.phoneNumber, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    id: [telephone.id]
                }));
            });
        }
        else {
            this.addPhone();
        }
    };
    ContactManageComponent.prototype.initTags = function (contact) {
        var _this = this;
        if (contact && contact.tags.length > 0) {
            var control_2 = this.form.controls['tags'];
            contact.tags.forEach(function (tag) {
                control_2.push(_this._fb.group({
                    tagName: [tag.tagName, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required],
                    id: [tag.id]
                }));
            });
        }
        else {
            this.addTag();
        }
    };
    ContactManageComponent.prototype.initEmails = function (contact) {
        var _this = this;
        if (contact && contact.emails.length > 0) {
            var control_3 = this.form.controls['emails'];
            contact.emails.forEach(function (email) {
                control_3.push(_this._fb.group({
                    emailAddress: [email.emailAddress, [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["d" /* Validators */].email]],
                    id: [email.id]
                }));
            });
        }
        else {
            this.addEmail();
        }
    };
    ContactManageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("../../../../../src/app/contact-app/contact-manage/contact-manage.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contact-app/contact-manage/contact-manage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["f" /* Location */]])
    ], ContactManageComponent);
    return ContactManageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/data/contact.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = (function () {
    function Contact(firstName, lastname, address, organization, telephones, emails, tags) {
        this.id = 0;
        this.firstName = firstName;
        this.lastName = lastname;
        this.organization = organization;
        this.telephones = telephones;
        this.emails = emails;
        this.address = address;
        this.tags = tags;
    }
    return Contact;
}());



/***/ }),

/***/ "../../../../../src/app/form-components/form.telephone.component.html":
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"phoneGroup\">\r\n    <div>\r\n        <label>PhoneNumber</label>\r\n        <input type=\"text\" formControlName=\"phoneNumber\">\r\n        <input type=\"hidden\" formControlName=\"id\">\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/form-components/form.telephone.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TelephoneComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TelephoneComponent = (function () {
    function TelephoneComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('phoneGroup'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */])
    ], TelephoneComponent.prototype, "phoneGroup", void 0);
    TelephoneComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'phone',
            template: __webpack_require__("../../../../../src/app/form-components/form.telephone.component.html")
        })
    ], TelephoneComponent);
    return TelephoneComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map