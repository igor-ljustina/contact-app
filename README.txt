angular app:
1: npm install
2: npm run build
3: npm run copy (copies the files to the MVC app)

mvc app:
1: Unzip (or create new) database
2: Run provided script upon the database. Script is located in ContactSPA.Data library: DataModelWithTags.edmx.sql
3: Modify Your connection string in web config to reflect DB location