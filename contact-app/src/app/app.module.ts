import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule }          from '@angular/forms';

import { AppComponent } from './app.component';
import { ContactListComponent } from './contact-app/contact-list/contact-list.component';
import { ContactDetailComponent } from './contact-app/contact-details/contact-details.component';
import { ContactManageComponent } from './contact-app/contact-manage/contact-manage.component';
import {TelephoneComponent} from './form-components/form.telephone.component';

const routes: Routes = [
  { path: '', component: ContactListComponent },
  { path: 'detail', component: ContactDetailComponent },
  { path: 'manage', component: ContactManageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactDetailComponent,
    ContactManageComponent,
    TelephoneComponent
  ],
  imports: [
    BrowserModule,    
    RouterModule.forRoot(routes, { useHash: true }),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
