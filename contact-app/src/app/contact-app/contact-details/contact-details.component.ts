import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from '../../data/contact.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    templateUrl: './contact-details.component.html',
    styleUrls: ['./contact-details.component.css']
})

export class ContactDetailComponent implements OnInit {
    private contactId;
    private contact: Contact;

    constructor(private route: ActivatedRoute, private http: HttpClient, private _location: Location) { }

    backClicked() {
        this._location.back();
    }

    deleteContact(id: number) {
        if (id) {
            if (confirm('Are You sure?')) {
                this.http.delete("/contact/DeleteContact/" + id).subscribe(res => {
                    console.log(res);
                }, err => {
                    console.log("Error occured");
                });
            }
        }
    }

    ngOnInit() {
        this.route
            .queryParams
            .subscribe(params => {
                this.contactId = params['id'];
            });
        if (this.contactId) {
            this.http.get<Contact>('/contact/GetContactDetails/' + this.contactId).subscribe(data => {
                this.contact = data;
                console.log('details', data);
            });
        }
    }
}