import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from '../../data/contact.model';

@Component({
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})

export class ContactListComponent implements OnInit {
  contactsList: Contact[];
  tagSearch: string = '';
  nameSearch: string = '';

  constructor(private http: HttpClient) { }

  onKey(event: any) {
    this.nameSearch = event.target.value;
    this.http.get<Contact[]>('/contact/GetContacts?search='+ this.nameSearch + '&tags=' + this.tagSearch).subscribe(data => {
      this.contactsList = data;
    });
  }
  onTagKey(event: any) {
    this.tagSearch = event.target.value;
    this.http.get<Contact[]>('/contact/GetContacts?search='+ this.nameSearch + '&tags=' + this.tagSearch).subscribe(data => {
      this.contactsList = data;
    });
  }
  ngOnInit(): void {
    // Make the HTTP request:
    this.http.get<Contact[]>('/contact/GetContacts?search='+ this.nameSearch + '&tags=' + this.tagSearch).subscribe(data => {
      this.contactsList = data;
    });
  }
}
