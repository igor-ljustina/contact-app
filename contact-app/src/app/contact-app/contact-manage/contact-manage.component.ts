import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from '../../data/contact.model';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
    templateUrl: './contact-manage.component.html',
    styleUrls: ['./contact-manage.component.css']
})

export class ContactManageComponent implements OnInit {
    private contactId;
    loadedContact: Contact;
    form: FormGroup;
    post: any;

    constructor(private _fb: FormBuilder, private route: ActivatedRoute, private http: HttpClient, private _location: Location) { }


    onSubmit(post) {
        if (this.contactId) {
            this.loadedContact.emails = post.emails;
            this.loadedContact.telephones = post.telephones;
            this.loadedContact.firstName = post.firstName;
            this.loadedContact.lastName = post.lastName;
            this.loadedContact.organization = post.organization;
            this.loadedContact.address = post.address;
            this.loadedContact.tags = post.tags;
            this.http.post('/contact/UpdateContact', this.loadedContact).subscribe(res => {
                console.log('updated', res);
            },
                err => {
                    console.log("Error occured:" + err);
                });
        }
        else {
            this.loadedContact = new Contact(post.firstName, post.lastName, post.address, post.organization, post.telephones, post.emails, post.tags);
            this.http.post('/contact/CreateContact', this.loadedContact).subscribe(res => {
                console.log(res);
            },
                err => {
                    console.log("Error occured");
                });
        }

        console.log(this.loadedContact);

    }
    ngOnInit() {
        this.route
            .queryParams
            .subscribe(params => {
                this.contactId = params['id'];
            });

        if (this.contactId) {
            this.http.get<Contact>('/contact/GetContactDetails/' + this.contactId).subscribe(data => {
                this.loadedContact = data;

                this.form = this._fb.group({
                    firstName: [this.loadedContact.firstName, Validators.required],
                    lastName: [this.loadedContact.lastName, Validators.required],
                    organization: [this.loadedContact.organization, Validators.required],
                    address: [this.loadedContact.address, Validators.required],
                    telephones: this._fb.array([]),
                    emails: this._fb.array([]),
                    tags: this._fb.array([])
                });
                this.initEmails(this.loadedContact);
                this.initTelephones(this.loadedContact);
                this.initTags(this.loadedContact);
            });
        }
        else {
            this.form = this._fb.group({
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                organization: ['', Validators.required],
                address: ['', Validators.required],
                telephones: this._fb.array([]),
                emails: this._fb.array([]),
                tags: this._fb.array([])
            });
            this.initEmails(null);
            this.initTelephones(null);
            this.initTags(null);
        }
    }

    addPhone() {
        const control = <FormArray>this.form.controls['telephones'];
        control.push(this._fb.group({
            phoneNumber: [''],
            id: [0]
        }));
    }

    addEmail() {
        const control = <FormArray>this.form.controls['emails'];
        control.push(this._fb.group({
            emailAddress: [''],
            id: [0]
        }));
    }

    addTag() {
        const control = <FormArray>this.form.controls['tags'];
        control.push(this._fb.group({
            tagName: [''],
            id: [0]
        }));
    }

    removeFromArray(i: number, array: string) {
        const control = <FormArray>this.form.controls[array];
        control.removeAt(i);
    }

    initTelephones(contact: Contact) {
        if (contact && contact.telephones.length > 0) {
            const control = <FormArray>this.form.controls['telephones'];
            contact.telephones.forEach(telephone => {
                control.push(this._fb.group({
                    phoneNumber: [telephone.phoneNumber, Validators.required],
                    id: [telephone.id]
                }));
            });
        } else {
            this.addPhone();
        }
    }

    initTags(contact: Contact) {
        if (contact && contact.tags.length > 0) {
            const control = <FormArray>this.form.controls['tags'];
            contact.tags.forEach(tag => {
                control.push(this._fb.group({
                    tagName: [tag.tagName, Validators.required],
                    id: [tag.id]
                }))
            })
        } else {
            this.addTag();
        }
    }

    initEmails(contact: Contact) {
        if (contact && contact.emails.length > 0) {
            const control = <FormArray>this.form.controls['emails'];
            contact.emails.forEach(email => {
                control.push(this._fb.group({
                    emailAddress: [email.emailAddress, [Validators.required, Validators.email]],
                    id: [email.id]
                }))
            })
        } else {
            this.addEmail();
        }
    }
}
