import {Telephone} from './telephone.model';
import {Email} from './email.model';

export class Contact {
    public key: string;
    public id: number;
    public firstName: string;
    public lastName: string;
    public address: string;
    public organization: string;
    public telephones: Array<Telephone>;
    public emails: Array<Email>;
    public tags: Array<{ id: string, tagName: string }>;

    constructor(
        firstName: string,
        lastname: string,
        address: string,
        organization: string,
        telephones: Array<Telephone>,
        emails: Array<Email>,
        tags: Array<{ id: string, tagName: string }>) {
        this.id = 0;
        this.firstName = firstName;
        this.lastName = lastname;
        this.organization = organization;
        this.telephones = telephones;
        this.emails = emails;
        this.address = address;
        this.tags = tags;
    }
}