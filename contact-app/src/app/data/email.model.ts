export class Email {
    public id: string;
    public emailAddress: string;
    constructor(id: string, emailAddress: string) {
        this.id = id;
        this.emailAddress = emailAddress;
    }
}