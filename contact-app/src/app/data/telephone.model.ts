export class Telephone {
    public id: string;
    public phoneNumber: string;
    constructor(id: string, phoneNumber: string) {
        this.id = id;
        this.phoneNumber = phoneNumber;
    }
}