import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'phone',
    templateUrl: 'form.telephone.component.html'
})
export class TelephoneComponent {
    // we will pass in address from App component
    @Input('phoneGroup')
    public phoneGroup: FormGroup;
}
